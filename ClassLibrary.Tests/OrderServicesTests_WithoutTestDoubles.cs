﻿using NUnit.Framework;

namespace OrderSystem.Tests
{
    [TestFixture]
    public class OrderServicesTests_WithoutTestDoubles
    {
        [Test]
        public void CalculateTotal_WithoutCoupon_ReturnLineItemTotal()
        {
            Order order = new Order { ItemTotal = 100 };
            OrderServices orderServices = new OrderServices(new DataAccess());

            var total = orderServices.CalculateTotal(order);

            Assert.AreEqual(100, total);
        }

        [Test]
        public void CalculateTotal_WithCoupon_ReturnLineItemWithDiscount()
        {
            Order order = new Order { Coupon = "christmas", ItemTotal = 100 };
            OrderServices orderServices = new OrderServices(new DataAccess());

            var total = orderServices.CalculateTotal(order);

            Assert.AreEqual(90, total);
        }

        [Test]
        public void Save_ValidOrder_TheOrderIsPersisted()
        {
            Order order = new Order { Id = 1, ItemTotal = 100, Total = 110 };
            OrderServices orderProcessor = new OrderServices(new DataAccess());

            orderProcessor.Save(order);

            Order orderFromDb = orderProcessor.GetOrder(order.Id);
            Assert.IsNotNull(orderFromDb);
        }
    }

}
