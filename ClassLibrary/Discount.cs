﻿namespace OrderSystem
{
    public class Discount
    {
        public string Coupon { get; set; }
        public int Percentage { get; set; }
    }
}
